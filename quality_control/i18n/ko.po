# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* quality_control
# 
# Translators:
# Martin Trigaux, 2019
# JH CHOI <hwangtog@gmail.com>, 2019
# Link Up링크업 <linkup.way@gmail.com>, 2019
# Linkup <link-up@naver.com>, 2019
# Seongseok Shin <shinss61@hotmail.com>, 2019
# Mark Lee <odoos@soti.33mail.com>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-26 08:04+0000\n"
"PO-Revision-Date: 2019-08-26 09:37+0000\n"
"Last-Translator: Mark Lee <odoos@soti.33mail.com>, 2019\n"
"Language-Team: Korean (https://www.transifex.com/odoo/teams/41243/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "% of the operation"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_dashboard_view_kanban
msgid "<i class=\"fa fa-ellipsis-v\" role=\"img\" aria-label=\"Manage\" title=\"Manage\"/>"
msgstr "<i class=\"fa fa-ellipsis-v\" role=\"img\" aria-label=\"Manage\" title=\"Manage\"/>"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_dashboard_view_kanban
msgid ""
"<i class=\"fa fa-envelope-o\" role=\"img\" aria-label=\"Domain alias\" "
"title=\"Domain alias\"/>&amp;nbsp;"
msgstr ""
"<i class=\"fa fa-envelope-o\" role=\"img\" aria-label=\"Domain alias\" "
"title=\"Domain alias\"/>&amp;nbsp;"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid ""
"<span attrs=\"{'invisible': [('measure_frequency_type', '=', "
"'all')]}\">Every </span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid ""
"<span class=\"fa fa-2x\" data-icon=\"∑\" style=\"padding-left: 10px;\" "
"role=\"img\" aria-label=\"Statistics\" title=\"Statistics\"/>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.stock_picking_view_form_inherit_quality
msgid ""
"<span class=\"o_stat_text text-success\" attrs=\"{'invisible': [('quality_check_fail', '=', True)]}\">Quality Checks</span>\n"
"                    <span class=\"o_stat_text text-danger\" attrs=\"{'invisible': [('quality_check_fail', '!=', True)]}\">Quality Checks</span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid ""
"<span class=\"o_stat_text\">AVG:</span>\n"
"                        <span class=\"o_stat_text\">STD:</span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.product_product_form_view_quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.product_template_form_view_quality_control
msgid ""
"<span class=\"o_stat_text\">Pass:</span>\n"
"                        <span class=\"o_stat_text\">Fail:</span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.stock_picking_view_form_inherit_quality
msgid "<span class=\"o_stat_text\">Quality Alert</span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "<span class=\"o_stat_text\">Quality Check</span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "<span>from </span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "<span>to </span>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.view_quality_point_kanban
msgid "<strong>Operation :</strong>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.view_quality_point_kanban
msgid "<strong>Product :</strong>"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_view_form
msgid "Accept Emails From"
msgstr "이메일 허용"

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_tag_action
msgid "Add a new tag"
msgstr "새 태그 추가"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_stock_picking__quality_alert_ids
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form
msgid "Alerts"
msgstr "알림"

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_point__measure_frequency_type__all
msgid "All Operations"
msgstr "전체 작업"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Archived"
msgstr "보관"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_point__average
msgid "Average"
msgstr "평균"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_small
msgid "Cancel"
msgstr "취소"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_tree
msgid "Checked By"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_tree
msgid "Checked Date"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_stock_picking__check_ids
msgid "Checks"
msgstr "확인"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_dashboard_view_kanban
msgid "Checks In Progress"
msgstr ""

#. module: quality_control
#: model:ir.ui.menu,name:quality_control.menu_quality_configuration
msgid "Configuration"
msgstr "환경 설정"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_failure
msgid "Confirm Measure"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "Control Frequency"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form
msgid "Control Person"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Control Point"
msgstr ""

#. module: quality_control
#: model:ir.ui.menu,name:quality_control.menu_quality_control_points
msgid "Control Points"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_tree
msgid "Control Type"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_failure
msgid "Correct Measure"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "Corrective Actions"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_alert_action_check
msgid "Create a new quality alert"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_alert_stage_action
msgid "Create a new quality alert stage"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_check_action_main
#: model_terms:ir.actions.act_window,help:quality_control.quality_check_action_team
msgid "Create a new quality check"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_point_action
msgid "Create a new quality control point"
msgstr ""

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_point__measure_frequency_unit__day
msgid "Days"
msgstr "일"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "Describe the corrective actions you did..."
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "Describe the preventive actions you did..."
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "Description"
msgstr "설명"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "Description of the issue..."
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_view_form
msgid "Email Alias"
msgstr "이메일 별칭"

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_check__measure_success__fail
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_small
msgid "Fail"
msgstr "실패"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
msgid "Failed"
msgstr "실패함"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_check__failure_message
#: model:ir.model.fields,field_description:quality_control.field_quality_point__failure_message
msgid "Failure Message"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "Frequency"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_point__measure_frequency_unit_value
msgid "Frequency Unit Value"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Group By"
msgstr "그룹화"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_dashboard_view_kanban
msgid "In #{kanban_getcolorname(record.color.raw_value)}"
msgstr "In #{kanban_getcolorname(record.color.raw_value)}"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
msgid "In Progress"
msgstr "진행 중"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form
msgid "Make Alert"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_check__tolerance_max
#: model:ir.model.fields,field_description:quality_control.field_quality_point__tolerance_max
msgid "Max Tolerance"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_check__measure
msgid "Measure"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_point__measure_frequency_unit
msgid "Measure Frequency Unit"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_check__measure_success
msgid "Measure Success"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "Message If Failure"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_check__tolerance_min
#: model:ir.model.fields,field_description:quality_control.field_quality_point__tolerance_min
msgid "Min Tolerance"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "Miscellaneous"
msgstr "기타"

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_point__measure_frequency_unit__month
msgid "Months"
msgstr "월"

#. module: quality_control
#: code:addons/quality_control/models/quality.py:0
#, python-format
msgid "New"
msgstr "신규"

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_check__measure_success__none
msgid "No measure"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_alert_action_report
msgid "No quality alert"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_check_action_report
msgid "No quality checks found"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_point__norm
msgid "Norm"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_check__norm_unit
#: model:ir.model.fields,field_description:quality_control.field_quality_point__norm_unit
msgid "Norm Unit"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_check_action_main
#: model_terms:ir.actions.act_window,help:quality_control.quality_check_action_team
msgid ""
"Note that the easiest way to create a quality check is to do it directly from a logistic operation,\n"
"              thanks to the use of quality control points."
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form
msgid "Notes"
msgstr "노트"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_failure_message
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_failure
msgid "OK"
msgstr "확인"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Operation Type"
msgstr "생산 관리 유형"

#. module: quality_control
#: model:ir.ui.menu,name:quality_control.menu_quality_dashboard
msgid "Overview"
msgstr "입출고현황"

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_check__measure_success__pass
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_small
msgid "Pass"
msgstr "통과"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
msgid "Passed"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_stock_picking__quality_check_todo
msgid "Pending checks"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_point__measure_frequency_value
msgid "Percentage"
msgstr "퍼센트"

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_point__measure_frequency_type__periodical
msgid "Periodically"
msgstr ""

#. module: quality_control
#: code:addons/quality_control/models/quality.py:0
#, python-format
msgid "Picture Uploaded"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_form
msgid "Preventive Actions"
msgstr ""

#. module: quality_control
#: model:ir.model,name:quality_control.model_product_product
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Product"
msgstr "상품"

#. module: quality_control
#: model:ir.model,name:quality_control.model_product_template
msgid "Product Template"
msgstr "상품 서식"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_view_form
msgid "Project Name"
msgstr "프로젝트 이름"

#. module: quality_control
#: model:ir.ui.menu,name:quality_control.menu_quality_root
msgid "Quality"
msgstr "품질 관리"

#. module: quality_control
#: code:addons/quality_control/models/quality.py:0
#: code:addons/quality_control/models/quality.py:0
#: model:ir.model,name:quality_control.model_quality_alert
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_calendar
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_search_inherit_quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.stock_picking_view_form_inherit_quality
#, python-format
msgid "Quality Alert"
msgstr "품질 경고"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_graph
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_view_pivot
msgid "Quality Alert Analysis"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_stock_picking__quality_alert_count
msgid "Quality Alert Count"
msgstr "품질 경고 횟수"

#. module: quality_control
#: model:ir.actions.act_window,name:quality_control.quality_alert_stage_action
#: model:ir.ui.menu,name:quality_control.menu_quality_config_alert_stage
msgid "Quality Alert Stages"
msgstr ""

#. module: quality_control
#: model:ir.model,name:quality_control.model_quality_alert_team
msgid "Quality Alert Team"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_alert_stage_action
msgid ""
"Quality Alert stages define the different steps a quality alert should go "
"through."
msgstr ""

#. module: quality_control
#: model:ir.actions.act_window,name:quality_control.quality_alert_action_check
#: model:ir.actions.act_window,name:quality_control.quality_alert_action_report
#: model:ir.actions.act_window,name:quality_control.quality_alert_action_team
#: model:ir.ui.menu,name:quality_control.menu_quality_alert
#: model:ir.ui.menu,name:quality_control.menu_quality_alert_report
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_dashboard_view_kanban
msgid "Quality Alerts"
msgstr ""

#. module: quality_control
#: code:addons/quality_control/models/quality.py:0
#: model:ir.model,name:quality_control.model_quality_check
#, python-format
msgid "Quality Check"
msgstr "품질 검사"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_graph
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_pivot
msgid "Quality Check Analysis"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_stock_picking__quality_check_fail
msgid "Quality Check Fail"
msgstr "품질 검사 실패"

#. module: quality_control
#: code:addons/quality_control/models/quality.py:0
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_failure
#, python-format
msgid "Quality Check Failed"
msgstr ""

#. module: quality_control
#: code:addons/quality_control/models/quality.py:0
#: model:ir.actions.act_window,name:quality_control.quality_check_action_main
#: model:ir.actions.act_window,name:quality_control.quality_check_action_picking
#: model:ir.actions.act_window,name:quality_control.quality_check_action_report
#: model:ir.actions.act_window,name:quality_control.quality_check_action_small
#: model:ir.actions.act_window,name:quality_control.quality_check_action_team
#: model:ir.ui.menu,name:quality_control.menu_quality_check_report
#: model:ir.ui.menu,name:quality_control.menu_quality_checks
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_small
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.stock_picking_view_form_inherit_quality
#, python-format
msgid "Quality Checks"
msgstr ""

#. module: quality_control
#: model:ir.actions.act_window,name:quality_control.quality_check_action_spc
msgid "Quality Checks SPC"
msgstr ""

#. module: quality_control
#: model:ir.ui.menu,name:quality_control.menu_quality_control
msgid "Quality Control"
msgstr "품질 관리"

#. module: quality_control
#: model:ir.model,name:quality_control.model_quality_point
msgid "Quality Control Point"
msgstr "품질 관리점"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_product_product__quality_control_point_qty
#: model:ir.model.fields,field_description:quality_control.field_product_template__quality_control_point_qty
msgid "Quality Control Point Qty"
msgstr ""

#. module: quality_control
#: model:ir.actions.act_window,name:quality_control.quality_point_action
msgid "Quality Control Points"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_product_product__quality_fail_qty
#: model:ir.model.fields,field_description:quality_control.field_product_template__quality_fail_qty
msgid "Quality Fail Qty"
msgstr ""

#. module: quality_control
#: model:ir.actions.act_window,name:quality_control.quality_alert_team_action
msgid "Quality Overview"
msgstr ""

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_product_product__quality_pass_qty
#: model:ir.model.fields,field_description:quality_control.field_product_template__quality_pass_qty
msgid "Quality Pass Qty"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.product_product_form_view_quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.product_template_form_view_quality_control
msgid "Quality Points"
msgstr ""

#. module: quality_control
#: model:ir.actions.act_window,name:quality_control.quality_tag_action
#: model:ir.ui.menu,name:quality_control.menu_config_quality_tags
msgid "Quality Tags"
msgstr ""

#. module: quality_control
#: model:ir.actions.act_window,name:quality_control.quality_alert_team_action_config
#: model:ir.ui.menu,name:quality_control.menu_quality_config_alert_team
msgid "Quality Teams"
msgstr ""

#. module: quality_control
#: model_terms:ir.actions.act_window,help:quality_control.quality_alert_team_action
msgid ""
"Quality Teams group the different quality alerts/checks\n"
"              according to the roles (teams) that need them."
msgstr ""

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_point__measure_frequency_type__random
msgid "Randomly"
msgstr "무작위로"

#. module: quality_control
#: model:ir.ui.menu,name:quality_control.menu_quality_reporting
msgid "Reporting"
msgstr "보고"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Responsible"
msgstr "담당자"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_dashboard_view_kanban
msgid "Settings"
msgstr "환경 설정"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_stage_view_tree
msgid "Stage Name"
msgstr "단계명"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_point__standard_deviation
msgid "Standard Deviation"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
msgid "Status"
msgstr "단계"

#. module: quality_control
#: model:ir.model,name:quality_control.model_stock_move
msgid "Stock Move"
msgstr "재고 이동"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_tag_view_search
#: model_terms:ir.ui.view,arch_db:quality_control.quality_tag_view_tree
msgid "Tags"
msgstr "태그"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_search
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Team"
msgstr "팀"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_alert_team_view_tree
msgid "Teams"
msgstr "팀"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_search
msgid "Test Type"
msgstr "테스트 유형"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_alert__title
msgid "Title"
msgstr "제목"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "Tolerance"
msgstr ""

#. module: quality_control
#: model:ir.model,name:quality_control.model_stock_picking
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_tree
msgid "Transfer"
msgstr "이동"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_point__measure_frequency_type
msgid "Type of Frequency"
msgstr ""

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_small
#: model_terms:ir.ui.view,arch_db:quality_control.quality_point_view_form_inherit_quality_control
msgid "Unit of Measure"
msgstr "단위"

#. module: quality_control
#: model_terms:ir.ui.view,arch_db:quality_control.quality_check_view_form_small
msgid "Validate"
msgstr "검증"

#. module: quality_control
#: model:ir.model.fields,field_description:quality_control.field_quality_check__warning_message
msgid "Warning Message"
msgstr ""

#. module: quality_control
#: model:ir.model.fields.selection,name:quality_control.selection__quality_point__measure_frequency_unit__week
msgid "Weeks"
msgstr "주"

#. module: quality_control
#: code:addons/quality_control/models/quality.py:0
#, python-format
msgid "You measured %.2f %s and it should be between %.2f and %.2f %s."
msgstr ""

#. module: quality_control
#: code:addons/quality_control/models/stock_picking.py:0
#, python-format
msgid "You still need to do the quality checks!"
msgstr "품질 검사를 해야 합니다!"
